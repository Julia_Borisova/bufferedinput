package jbs;

import java.io.*;


/**
 * Класс, который читает данные из файла text.txt, выполняет действие и записывает результат в файл results.txt
 */
public class BufferedInput {
    public static void main(String[] args) throws IOException {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("results.txt"))){
        try (BufferedReader reader = new BufferedReader(new FileReader("text.txt"))) {

            String string;
            int result = 0;
            while ((string = reader.readLine()) != null) {
                String[] strings = string.split(" ");
                int numberOne = Integer.valueOf(strings[0]);
                int numberToo = Integer.valueOf(strings[2]);

                switch (strings[1]) {
                    case "+":
                        result = numberOne + numberToo;
                        break;
                    case "-":
                        result = numberOne - numberToo;
                        break;
                    case "*":
                        result = numberOne * numberToo;
                        break;
                    case ":":
                        result = numberOne / numberToo;
                    default:
                        break;
                }
                writer.write(string + " = " + result + "\n");
            }
        } catch (IOException e){
            System.err.println("Не удается найти указанный файл.");
            System.exit(0);
        }

        } catch (IOException e){
            System.err.println("Встречены некорректные данные.");
        }
    }

}
